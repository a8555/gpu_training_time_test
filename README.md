# Argos dockerized

Step 0:
If you are rerunning our test or rebuilding our Docker container you can skip to step 3, but you might want to delete the previous Docker image by running:
sudo docker image rm -f lung

Step 1:  
If you have not already done so, install an nvidia driver:  
	Step 1: Identify the type of GPU in your system by using the console and typing: ubuntu-drivers devices  
	Step 2: Your GPU and a list of drivers should appear. Please make a printscreen of this output and send it to us. We recommend 				installing the latest recommended version or a version >= 418.81.07.  
			For example on a Tesla M60 we would type: sudo apt install nvidia-driver-490  
	Step 3: Reboot your Ubuntu machine.

Step 2:  
The next step is to install Docker-CE by typing the commands below or follow the official instructions here (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker):   
curl https://get.docker.com | sh \
&& sudo systemctl --now enable docker

Next we install the NVIDIA Container toolkit:  
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

Update: sudo apt-get update  
Install the nvidia docker container: sudo apt-get install -y nvidia-docker2  
Restart the docker daemon: sudo systemctl restart docker  
We can test if the installation was succesful by typing: sudo docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi  


Step 3:  
Download and unzip the argos-dockerized-main.zip  
Change your working directory to the argos-dockerized-main folder: cd your_path_here/argos-dockerized-main  
Next we build the container: sudo docker build -f Dockerfile -t lung .  

Run on GPU:  
sudo docker run --gpus all -v -ti lung python -u run_online.py  
sudo docker run --gpus all -v -ti lung python -u predict.py  

